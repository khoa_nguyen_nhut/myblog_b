import { User } from './../model/User';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  constructor(public accountService: AccountService) { }

  ngOnInit(): void {
  }
  logout() {
    this.accountService.logout();
  }
}
