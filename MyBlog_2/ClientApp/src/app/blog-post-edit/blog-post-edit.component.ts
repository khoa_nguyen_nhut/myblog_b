import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Post } from '../model/Post';
import { PostService } from '../post.service';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog-post-edit',
  templateUrl: './blog-post-edit.component.html',
  styleUrls: ['./blog-post-edit.component.css']
})
export class BlogPostEditComponent implements OnInit {
  PostForm = new FormGroup({
    title: new FormControl(),
    genre: new FormControl(),
    image: new FormControl(),
    summary: new FormControl(),
    content: new FormControl(),
  });
  post: Post;
  selectedFile: File;
  constructor(private postService: PostService, private accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
    this.post = window.history.state;
    this.PostForm = new FormGroup({
      title: new FormControl(this.post.title),
      genre: new FormControl(this.post.genre),
      image: new FormControl(this.post.image),
      summary: new FormControl(this.post.summary),
      content: new FormControl(this.post.content),
    });
  }
  onFileChange(event) {
    this.selectedFile = (event.target.files[0] as File);
  }
  submitPost() {
    this.post = {
      id: this.post.id,
      username: this.accountService.getCurrentUserValue().username,
      title: this.PostForm.get('title').value,
      image: '',
      genre: this.PostForm.get('genre').value,
      date: this.post.date,
      summary: this.PostForm.get('summary').value,
      content: this.PostForm.get('content').value,
      comments: new Array()
    };
    if (this.selectedFile != null) {
      this.post.image = this.selectedFile.name;
      this.postService.uploadImage(this.selectedFile);
    }
    this.postService.submitEdit(this.post);
    this.router.navigate(['/posts/' + this.post.id], { state: null});
  }
}
