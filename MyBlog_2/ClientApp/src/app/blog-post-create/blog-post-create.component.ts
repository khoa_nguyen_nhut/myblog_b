import { AccountService } from './../account.service';
import { PostService } from './../post.service';
import { Post } from 'src/app/model/Post';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-blog-post-create',
  templateUrl: './blog-post-create.component.html',
  styleUrls: ['./blog-post-create.component.css']
})
export class BlogPostCreateComponent implements OnInit {
  PostForm = new FormGroup({
    title: new FormControl(),
    genre: new FormControl(),
    image: new FormControl(),
    summary: new FormControl(),
    content: new FormControl(),
  });
  post: Post;
  selectedFile: File;
  constructor(private postService: PostService, private accountService: AccountService) { }

  ngOnInit(): void {
  }
  onFileChange(event) {
    this.selectedFile = (event.target.files[0] as File);
  }
  submitPost() {
    this.post = {
      id: 0,
      username: this.accountService.getCurrentUserValue().username,
      title: this.PostForm.get('title').value,
      image: '',
      genre: this.PostForm.get('genre').value,
      date: new Date(),
      summary: this.PostForm.get('summary').value,
      content: this.PostForm.get('content').value,
      comments: new Array()
    };
    if (this.selectedFile != null) {
      this.post.image = this.selectedFile.name;
      this.postService.uploadImage(this.selectedFile);
    }
    if (this.post.title != null){
    this.postService.submitPost(this.post);
    this.PostForm.reset();
  }
  }
}
