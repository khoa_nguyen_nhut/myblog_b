import { TopBarComponent } from './../top-bar/top-bar.component';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/User';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  user: User;
  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
    isRemember: new FormControl()
  });
  constructor(
    public accountService: AccountService,
    private router: Router) { }
  ngOnInit(): void {
  }

  submit() {
    this.user = {
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value,
      email: '',
      role: '',
      phoneNumber: 0,
      image: '',
    };
    this.accountService.login(this.user);
  }
  register() {
    if (this.accountService.getCurrentUserValue() == null) {
      this.router.navigate(['/register']);
    }
  }
  isLoggedIn() {
    if (this.accountService.getCurrentUserValue() != null) {
      this.router.navigate(['']);
    }
  }
}
