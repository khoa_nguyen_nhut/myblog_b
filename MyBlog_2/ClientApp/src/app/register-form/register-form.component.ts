import { AccountService } from './../account.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  registerForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
    email: new FormControl(),
    role: new FormControl('user'),
    phoneNumber: new FormControl(0),
  });
  user: User;
  isUserExist: string;
  selectedFile: File;
  constructor(
    private accountService: AccountService,
    private router: Router) { }

  ngOnInit(): void {
  }
  onUserNameChange() {
    this.isUserExist = '';
    this.accountService.getUser(this.registerForm.get('username').value).subscribe(result => {
      if (result) {
        this.isUserExist = 'User is existed!';
      }
    });
  }
  onFileChange(event) {
    this.selectedFile = (event.target.files[0] as File);
  }
  submit() {
    this.user = {
      username: this.registerForm.get('username').value,
      password: this.registerForm.get('password').value,
      email: this.registerForm.get('email').value,
      role: this.registerForm.get('role').value,
      phoneNumber: this.registerForm.get('phoneNumber').value,
      image: ''
    };
    if (this.selectedFile != null) {
      this.user.image = this.selectedFile.name;
      this.accountService
        .uploadImage(this.selectedFile);
    }
    if (this.user.username != null || this.user.password != null) {
      this.accountService.register(this.user);
      this.router.navigate(['']);
    }
  }

}
