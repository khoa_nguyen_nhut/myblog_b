import { Post } from './../model/Post';
import { AccountService } from './../account.service';
import { Comment } from './../model/Comment';
import { PostService } from '../post.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-blog-post-detail',
  templateUrl: './blog-post-detail.component.html',
  styleUrls: ['./blog-post-detail.component.css']
})
export class BlogPostDetailComponent implements OnInit {
  postID: number;
  post: Post;
  postList: Post[];
  postListLength: number;
  comment: Comment;
  userImages: string[];
  commentForm = new FormGroup({
    comment: new FormControl(''),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public accountService: AccountService,
    public postService: PostService) { }

  ngOnInit(): void {
    this.userImages = new Array();
    this.route.paramMap.subscribe(params => {
      this.postID = (parseInt(params.get('postid'), 10));
    });
    if (Object.keys(window.history.state).length <= 1) {
      this.initPostListByUrl();
    } else {
      this.initPostListByStateUrl();
    }
  }
  initPostListByUrl(){
    this.postService.getPosts().subscribe(result => {
      this.postList = result.reverse();
      this.postListLength = Object.keys(this.postList).length;
      this.post = this.postList.find(x => x.id === this.postID);
      this.initImage(this.post);
    });
  }
  initPostListByStateUrl(){
    this.postList = Object.values(window.history.state);
    this.postList.pop();
    this.postList = this.postList.reverse();
    this.postListLength = Object.keys(this.postList).length;
    this.post = this.postList.find(x => x.id === this.postID);
    this.initImage(this.post);
  }
  initImage(post: Post) {
    for (const iterator of post.comments) {
      this.accountService.getUser(iterator.username).subscribe(result => {
       iterator.image = result.image;
      });
    }
  }
  getImage(comment: Comment) {
    this.accountService.getUser(comment.username).subscribe(result => {
      comment.image = result.image;
    });
  }
  submitComment() {
    if (this.accountService.getCurrentUserValue() != null) {
      this.comment = {
        date: new Date(),
        commentContent: this.commentForm.get('comment').value,
        username: this.accountService.getCurrentUserValue().username,
        postID: this.route.snapshot.paramMap.get('postid'),
        image: '',
        isRemoved: false
      };
      this.post.comments.push(this.comment);
      this.postService.submitComment(this.post);
      this.getImage(this.comment);
      this.commentForm.reset();
    } else {
      this.router.navigate(['/login']);
    }
  }
  nextPost() {
    if (this.postList.find(x => x.id === this.postID + 1)) {
      this.postID++;
      this.post = this.postList.find(x => x.id === this.postID);
      this.initImage(this.post);
      this.router.navigate(['/posts/' + this.postID]);
    }
  }
  previousPost() {
    if (this.postList.find(x => x.id === this.postID - 1)) {
      this.postID--;
      this.post = this.postList.find(x => x.id === this.postID);
      this.initImage(this.post);
      this.router.navigate(['/posts/' + this.postID]);
    }
  }
  editPost(){
    this.router.navigateByUrl('/post-edit', { state: this.post});
  }
  deletePost(){
    this.postService.submitDelete(this.post);
    window.alert('This post' + this.post.title + 'deleted!');
  }
  deleteComment(comment: Comment){
    this.post.comments.find(x => x === comment).isRemoved = true;
    this.postService.deleteComment(comment);
  }
}
