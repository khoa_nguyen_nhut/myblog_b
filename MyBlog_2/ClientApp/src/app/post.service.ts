import { Comment } from './model/Comment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Post } from 'src/app/model/Post';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiUrl = 'https://localhost:44308/api/';
  public imageUrl = 'https://localhost:44308/BlogImages/';
  constructor(private http: HttpClient) { }
  public  getPosts(): Observable<Post[]>{
    return this.http.get<Post[]>(this.apiUrl + 'Post').pipe(
      catchError(this.errorHandler)
    );
  }
  // public getPost(id: number): Observable<Post>{
  //   return this.http.get<Post>(this.apiUrl + 'Post/' + id ).pipe(
  //     catchError(this.errorHandler)
  //   );
  // }
  public submitPost(post: Post){
    return this.http.post(this.apiUrl + 'Post' + '/Create', post, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  public submitEdit(post: Post){
    return this.http.post(this.apiUrl + 'Post' + '/Edit', post, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  public submitDelete(post: Post){
    return this.http.post(this.apiUrl + 'Post' + '/Delete', post, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  public uploadImage(file: File){
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(this.apiUrl + 'Post' + '/Upload', formData, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }

  public submitComment(post: Post){
    this.http.post(this.apiUrl + 'Post' + '/SubmitComment', post, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  public deleteComment(comment: Comment){
    this.http.post(this.apiUrl + 'Post' + '/DeleteComment', comment, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }
}

