import { Post } from 'src/app/model/Post';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customFilter'
})
export class CustomFilterPipe implements PipeTransform {

  transform(PostList: Post[], term: string, selectedOption: string): any {
    if (term != null && PostList != null) {
      if (term !== ''){
      if (selectedOption === 'Title') {
        return PostList.filter(x => x.title.includes(term));
      }
      if (selectedOption === 'Genre') {
        return PostList.filter(x => x.genre === term);
      }
    }
    }
    return PostList;
  }
}
