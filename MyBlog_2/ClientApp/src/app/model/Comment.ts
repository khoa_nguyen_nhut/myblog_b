export interface Comment {
  date: Date;
  commentContent: string;
  username: string;
  postID: string;
  image: string;
  isRemoved: boolean;
}
