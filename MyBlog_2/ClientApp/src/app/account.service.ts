import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './model/User';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private currentUser: BehaviorSubject<User>;
  private apiUrl = 'https://localhost:44308/api/';
  public imageUrl = 'https://localhost:44308/Avatar/';
  constructor(private http: HttpClient) {
    this.currentUser = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
  }

  public getCurrentUserValue(): User {
    return this.currentUser.value;
}
  public login(user: User){
    this.http.post<User>(this.apiUrl + 'Account'  + '/Login', user).subscribe(result => {
      this.currentUser.next(result);
      localStorage.setItem('currentUser', JSON.stringify(result));
    }, error => console.error(error));
  }
  public register(user: User){
    this.http.post(this.apiUrl + 'Account' + '/Register', user, { responseType: 'text' }).subscribe(
   error => console.error(error));
  }
  public uploadImage(file: File){
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(this.apiUrl + 'Account' + '/Upload', formData, { responseType: 'text' }).subscribe(
      error => console.error(error));
  }
  public getUser(username: string): Observable<User>{
    return this.http.get<User>(this.apiUrl + 'Account/' + username).pipe(
      catchError(this.errorHandler)
    );
  }
  public logout(){
    localStorage.removeItem('currentUser');
    this.currentUser.next(null);
  }
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }
}
