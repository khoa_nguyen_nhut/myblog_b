import { AccountService } from './account.service';
import { PostService } from './post.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { BlogPostListComponent } from './blog-post-list/blog-post-list.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { BlogPostDetailComponent } from './blog-post-detail/blog-post-detail.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogPostCreateComponent } from './blog-post-create/blog-post-create.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { BlogPostEditComponent } from './blog-post-edit/blog-post-edit.component';
import { CustomFilterPipe } from './pipes/custom-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    BottomBarComponent,
    BlogPostListComponent,
    LoginFormComponent,
    AboutMeComponent,
    BlogPostDetailComponent,
    RegisterFormComponent,
    BlogPostCreateComponent,
    BlogPostEditComponent,
    CustomFilterPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  providers: [
    PostService,
    AccountService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
