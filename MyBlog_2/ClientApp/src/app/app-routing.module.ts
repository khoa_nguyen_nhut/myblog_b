import { BlogPostEditComponent } from './blog-post-edit/blog-post-edit.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { BlogPostCreateComponent } from './blog-post-create/blog-post-create.component';
import { BlogPostDetailComponent } from './blog-post-detail/blog-post-detail.component';
import { BlogPostListComponent } from './blog-post-list/blog-post-list.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';


const routes: Routes = [
  { path: '', component: BlogPostListComponent},
  { path: 'home', component: BlogPostListComponent},
  { path: 'login', component: LoginFormComponent},
  { path: 'register', component: RegisterFormComponent},
  { path: 'about-me', component: AboutMeComponent},
  { path: 'posts/:postid', component: BlogPostDetailComponent},
  { path: 'post-create', component: BlogPostCreateComponent, canActivate: [AuthGuard], data: { roles: ['admin'] }},
  { path: 'post-edit', component: BlogPostEditComponent, canActivate: [AuthGuard], data: { roles: ['admin'] }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
