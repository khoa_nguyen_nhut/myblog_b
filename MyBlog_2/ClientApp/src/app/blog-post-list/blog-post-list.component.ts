import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/model/Post';
import { PostService } from '../post.service';

@Component({
  selector: 'app-blog-post-list',
  templateUrl: './blog-post-list.component.html',
  styleUrls: ['./blog-post-list.component.css']
})
export class BlogPostListComponent implements OnInit {
  p = 1;
  postList: Post[];
  topPostList: Post[];
  categories: string[];
  selectedOption: string;
  term: string;
  constructor(public postService: PostService) { }

  ngOnInit(): void {
    this.selectedOption = 'Title';
    this.term = '';
    this.postService.getPosts().subscribe(result => {
      this.postList = result;
      this.topCommentFilter(this.postList);
    });
  }
  topCommentFilter(postList: Post[]) {
    this.topPostList = Object.assign([], postList);
    this.topPostList.sort((a, b) => (a.comments.length < b.comments.length) ? 1 : -1);
  }
  getCategories() {
    if (this.postList) {
      return Array.from(new Set(this.postList.map(data => data.genre)));
    }
  }
  setTerm(term: string): void {
    this.term = term;
    this.selectedOption = 'Genre';
  }
  resetPage() {
    this.p = 1;
  }

}

