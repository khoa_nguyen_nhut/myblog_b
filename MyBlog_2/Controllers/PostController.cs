﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog.Data;
using MyBlog.Models;

namespace MyBlog_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;
        private readonly MyBlogContext _blogContext;

        public PostController(IWebHostEnvironment environment, MyBlogContext blogContext)
        {
            _environment = environment;
            _blogContext = blogContext;
        }



        // GET: api/Post
        [HttpGet]
        public IEnumerable<PostModel> Get()
        {
            try
            {
                var PostList = _blogContext.Posts.Include(post => post.Comments).OrderByDescending(Post => Post.Date).ToList();
                return PostList;
            }
            catch (Exception)
            {
                return null;
            }

        }

        // GET: api/Post/5
        //[HttpGet("{id}", Name = "Get")]
        //public async Task<PostModel> GetAsync(int id)
        //{
        //    try
        //    {
        //        var Post = await _blogContext.Posts.Include(post => post.Comments).FirstOrDefaultAsync(m => m.ID == id);
        //        return Post;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }

        //}

        // POST: api/Post/Create
        [HttpPost("Create")]
        public IActionResult Post(PostModel post)
        {
            try
            {
                if (post.Image.Length < 1)
                {
                    post.Image = "m-farmerboy.jpg";
                }
                _blogContext.Add(post);
                _blogContext.SaveChanges();
                return Ok("Created!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }

        }
        // POST: api/Post/Edit
        [HttpPost("Edit")]
        public IActionResult EditPost(PostModel post)
        {
            try
            {
                if (post.Image.Length < 1)
                {
                    post.Image = "m-farmerboy.jpg";
                }
                _blogContext.Update(post);
                _blogContext.SaveChanges();
                return Ok("Edited Post!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }

        }
        // POST: api/Post/Delete
        [HttpPost("Delete")]
        public IActionResult DeletePost(PostModel post)
        {
            try
            {
                _blogContext.Posts.Remove(post);
                _blogContext.SaveChanges();
                return Ok("Edited Post!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }

        }
        // POST: api/Post/Upload
        [HttpPost("Upload"), DisableRequestSizeLimit]
        public IActionResult UploadPostImage()
        {
            try
            {
                var file = Request.Form.Files[0];
                if (file != null)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string uploadsFolder = Path.Combine(_environment.WebRootPath, "BlogImages");
                    string filePath = Path.Combine(uploadsFolder, fileName);
                    using var fileStream = new FileStream(filePath, FileMode.Create);
                    file.CopyTo(fileStream);
                }
                return Ok("PostImage Uploaded");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        // POST: api/Post/SubmitComment
        [HttpPost("SubmitComment")]
        public IActionResult SubmitComment(PostModel post)
        {
            try
            {
                var postDB = _blogContext.Posts.FirstOrDefault(x => x.ID.Equals(post.ID));
                var newComment = post.Comments.Last();
                if (postDB.Comments == null)
                {
                    postDB.Comments = new List<CommentModel>();
                }
                if (newComment != null)
                {
                    postDB.Comments.Add(newComment);
                }
                _blogContext.SaveChanges();
                return Ok("Submit success!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
        // POST: api/Post/DeleteComment
        [HttpPost("DeleteComment")]
        public IActionResult DeleteComment(CommentModel comment)
        {
            try
            {
                _blogContext.Comments.Remove(comment);
                _blogContext.SaveChanges();
                return Ok("Delete success!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
    }
}
