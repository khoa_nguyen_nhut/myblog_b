﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyBlog.Data;
using MyBlog.Models;
using System.Security.Cryptography;
using System.Text;

namespace MyBlog_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IWebHostEnvironment _environment;
        private readonly MyBlogContext _blogContext;

        public AccountController(IWebHostEnvironment environment, MyBlogContext blogContext)
        {
            _environment = environment;
            _blogContext = blogContext;
        }

        // POST: api/Account/Register
        [HttpPost("Register")]
        public IActionResult Register(UserModel user)
        {
            try
            {
                if (user.Username == null || user.Password == null)
                {
                    return StatusCode(500, $"Input user and password"); ;
                }
                if (user.Image == String.Empty)
                {
                    user.Image = "user-03.png";
                }
                user.Password = GetMd5Hash(user.Password);
                _blogContext.Add(user);
                _blogContext.SaveChanges();
                return Ok("Registered!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        // POST: api/Account/Login
        [HttpPost("Login")]
        public UserModel Login(UserModel user)
        {
            try
            {
                if (user.Username == null || user.Password == null)
                {
                    return null;
                }
                string hashedPassword = GetMd5Hash(user.Password);
                var result = _blogContext.Users.FirstOrDefault(u => u.Username.Equals(user.Username) && VerifyMd5Hash(user.Password, hashedPassword));
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        // POST: api/Account/Upload
        [HttpPost("Upload"), DisableRequestSizeLimit]
        public IActionResult UploadUserImage()
        {
            try
            {
                var file = Request.Form.Files[0];
                if (file != null)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string uploadsFolder = Path.Combine(_environment.WebRootPath, "Avatar");
                    string filePath = Path.Combine(uploadsFolder, fileName);
                    using var fileStream = new FileStream(filePath, FileMode.Create);
                    file.CopyTo(fileStream);
                }
                return Ok("Uploaded!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
        // GET: api/Account/{Username}
        [HttpGet("{username}", Name = "GetUser")]
        public async Task<UserModel> GetUser(string username)
        {
            try
            {
                var user = await _blogContext.Users.FirstOrDefaultAsync(m => m.Username == username);
                return user;
            }
            catch (Exception)
            {
                return null;
            }

        }
        private static string GetMd5Hash(string password)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(password));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        private static bool VerifyMd5Hash(string password,string input)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(password);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, input))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

