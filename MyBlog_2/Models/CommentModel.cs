﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyBlog.Models
{
    public class CommentModel
    {

        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string CommentContent { get; set; }
        [ForeignKey("UserModel")]
        public string Username { get; set; }
        public CommentModel()
        {
        }

        public CommentModel(int iD, DateTime date, string commentContent, string username)
        {
            ID = iD;
            Date = date;
            CommentContent = commentContent;
            Username = username;
        }
    }
}
