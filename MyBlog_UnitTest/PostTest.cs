﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MyBlog.Data;
using MyBlog.Models;
using MyBlog_2.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MyBlog_UnitTest
{
    public class PostTest
    {
        private IWebHostEnvironment _environment;
        private MyBlogContext _blogContext;

        public PostTest()
        {
            var dummyDB = new DummyDataDBInitializer();
            dummyDB.Init("Server=SE130046;Database=TestPostDB;User Id=sa;Password=123;");
            _environment = dummyDB.environment;
            _blogContext = dummyDB.myBlogContext;
        }

        [Fact]
        public void Test01_CreatePost_Input_Return_OkObjectResult()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();

            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";

            //Act
            //Create Post
            var response = controller.Post(post);
            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test02_CreatePost_Input_Return_InternalServerError()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = null;

            //Act
            //Create Post
            var response = controller.Post(post);
            //Assert
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
        [Fact]
        public void Test03_EditPost_Input_Return_OkObjectResult()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();
            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            //Act
            //Create Post
            controller.Post(post);

            //Arrange
            post.Content = "aaa";
            post.Summary = "bbb";
            //Act
            //Edit Post
            var response = controller.EditPost(post);
            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test04_EditPost_Input_Return_InternalServerError()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();
            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            //Act
            //Create Post
            controller.Post(post);

            //Arrange
            post = new PostModel();

            post.ID = 4;
            post.Title = "test2";
            post.Genre = "aaaaaaaa";
            post.Image = "";
            post.Content = "ddd";
            post.Summary = "ccccc";

            //Act
            //Edit Post
            var response = controller.EditPost(post);

            //Assert
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
        [Fact]
        public void Test05_DeletePost_Input_Return_OkObjectResult()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();
            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            //Act
            //Create Post
            controller.Post(post);

            //Act
            //Delete Post
            var response = controller.DeletePost(post);

            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test06_DeletePost_Input_Return_InternalServerError()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();
            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            //Act
            //Create Post
            controller.Post(post);

            //Arrange
            post = new PostModel();

            post.ID = 4;
            post.Title = "test2";
            post.Genre = "aaaaaaaa";
            post.Image = "";
            post.Content = "ddd";
            post.Summary = "ccccc";

            //Act
            //Create Post
            var response = controller.DeletePost(post);

            //Assert
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
        [Fact]
        public void Test07_SubmitComment_Input_Return_OkObjectResult()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();

            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            post.Comments = new List<CommentModel>();
            CommentModel comment = new CommentModel();
            comment.Username = "123";
            comment.Date = DateTime.Now;
            comment.CommentContent = "This is test comment";
            post.Comments.Add(comment);
            //Act
            //Create Post
            controller.Post(post);

            //Act
            //commit Comment
            var response = controller.SubmitComment(post);

            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test08_SubmitComment_Input_Return_InternalServerError()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();

            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            //Act
            //Create Post
            controller.Post(post);

            //Act
            //commit Comment
            var response = controller.SubmitComment(post);

            //Assert
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
        [Fact]
        public void Test09_DeleteComment_Return_OkObjectResult()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();

            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            post.Comments = new List<CommentModel>();
            CommentModel comment = new CommentModel();
            comment.Username = "123";
            comment.Date = DateTime.Now;
            comment.CommentContent = "This is test comment";
            post.Comments.Add(comment);
            //Act
            //Create Post
            controller.Post(post);

            //Act
            //commit Comment
            controller.SubmitComment(post);

            //Act
            //Delete Comment
            var response = controller.DeleteComment(comment);

            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test10_DeleteComment_Return_InternalServerError()
        {
            //Arrange
            var controller = new PostController(_environment, _blogContext);
            PostModel post = new PostModel();

            post.Title = "test";
            post.Genre = "Genre Test";
            post.Image = "";
            post.Content = "asda";
            post.Summary = "asdas";
            post.Comments = new List<CommentModel>();
            CommentModel comment = new CommentModel();
            comment.Username = "123";
            comment.Date = DateTime.Now;
            comment.CommentContent = "This is test comment";
            post.Comments.Add(comment);
            //Act
            //Create Post
            controller.Post(post);

            //Act
            //commit Comment
            controller.SubmitComment(post);

            //Arrange
            comment = new CommentModel();
            //Act
            //Delete Comment
            var response = controller.DeleteComment(comment);

            //Assert
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
    }
}
