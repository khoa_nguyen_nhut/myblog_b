using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyBlog.Data;
using MyBlog.Models;
using MyBlog_2.Controllers;
using System;
using System.Net;
using Xunit;

namespace MyBlog_UnitTest
{
    public class AccountTest
    {
        private IWebHostEnvironment _environment;
        private MyBlogContext _blogContext;

        public AccountTest()
        {
            var dummyDB = new DummyDataDBInitializer();
            dummyDB.Init("Server=SE130046;Database=TestAccountDB;User Id=sa;Password=123;");
            _environment = dummyDB.environment;
            _blogContext = dummyDB.myBlogContext;
        }

        [Fact]
        public void Test01_RegisterAccount_Input_Return_OkResult()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;
            //Act
            //Register the user
            var response = controller.Register(user);
            //Assert
            Assert.IsType<OkObjectResult>(response);
        }
        [Fact]
        public void Test02_RegisterAccount_Input_Return_InternalServerError()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = null;
            user.Password = null;
            user.Role = null;
            user.Email = null;
            user.Phonenumber = 0;

            //Act
            //Register the user
            var response = controller.Register(user);
            Assert.IsType<ObjectResult>(response);
            var objectResponse = response as ObjectResult;
            //Assert
            Assert.Equal(500, objectResponse.StatusCode);
        }
        [Fact]
        public void Test03_LoginAccount_Input_Return_User()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;

            //Act
            //Register the user
            controller.Register(user);

            //Arrange
            //User input username and password
            user = new UserModel();
            user.Username = "123";
            user.Password = "123";
            //act
            //User login
            var respose = controller.Login(user);
            //Assert
            Assert.NotNull(respose);
        }

        [Fact]
        public void Test04_LoginAccount_Input_Invalid_Return_Null()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;

            //Act
            //Create User
            controller.Register(user);

            //Arrange
            //User input username and password
            user = new UserModel();
            user.Username = "321";
            user.Password = "321";

            //Act
            //User login
            var respose = controller.Login(user);
            //Assert
            Assert.Null(respose);
        }
        [Fact]
        public void Test05_LoginAccount_Input_Null_Return_Null()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;

            //Act
            //Create User
            controller.Register(user);

            //Arrange
            //User input username and password
            user = new UserModel();
            user.Username = String.Empty;
            user.Password = String.Empty;

            //Act
            //User login
            var respose = controller.Login(user);
            //Assert
            Assert.Null(respose);
        }

        [Fact]
        public void Test06_getUserAccount_Input_Return_User()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;

            //Act
            //Create User
            controller.Register(user);

            //Arrange
            //User input
            string username = "123";

            //Act
            //Get User
            var respose = controller.GetUser(username);

            //Assert
            Assert.Equal(user, respose.Result);
        }
        [Fact]
        public void Test07_getUserAccount_Input_Return_Null()
        {
            //Arrange
            var controller = new AccountController(_environment, _blogContext);
            UserModel user = new UserModel();

            user.Username = "123";
            user.Password = "123";
            user.Role = "admin";
            user.Email = "123@gmail.com";
            user.Phonenumber = 12312;
            controller.Register(user);
            string username = "321";

            //Act
            //Get User
            var respose = controller.GetUser(username);
            //Assert
            Assert.Null(respose.Result);
        }
    }
}
