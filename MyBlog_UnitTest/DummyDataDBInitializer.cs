﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using MyBlog.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyBlog_UnitTest
{
    public class DummyDataDBInitializer
    {
        public  IWebHostEnvironment environment;
        public  MyBlogContext myBlogContext;
        private  DbContextOptions<MyBlogContext> dbContextOptions { get; set; }
        private  string _connectionString;

        public void Init(string connectionString)
        {
            _connectionString = connectionString;
            dbContextOptions = new DbContextOptionsBuilder<MyBlogContext>()
              .UseSqlServer(connectionString)
              .Options;
            myBlogContext = new MyBlogContext(dbContextOptions);
            
            myBlogContext.Database.EnsureDeleted();
            myBlogContext.Database.EnsureCreated();
        }
    }
}
